<?php
namespace Keepper\Lib\Pdo\Interfaces;

interface PdoInterface {

	/**
	 * Подготавливает sql запрос
	 * @param $statement
	 * @param array $driverOptions
	 * @return mixed
	 */
	public function prepare ($statement, array $driverOptions = array()): PdoStatementInterface;

	/**
	 * Стартует транзакцию
	 * @throws \PDOException
	 */
	public function beginTransaction ();

	/**
	 * Завершает транзакцию с сохранением изменений
	 * @throws \PDOException
	 */
	public function commit ();

	/**
	 * Завершает транзакцию с откатом изменений
	 * @throws \PDOException
	 */
	public function rollBack ();

	/**
	 * Возвращает признак, того, что транзакция открыта
	 */
	public function inTransaction (): bool;

	/**
	 * Устанавливает опцию PDO
	 * @see https://secure.php.net/manual/ru/pdo.setattribute.php
	 * @param int $attribute
	 * @param mixed $value
	 * @return bool
	 */
	public function setAttribute (int $attribute, $value): bool;

	/**
	 * Выполняет запрос и возвращает количество затронутых строк
	 * @param string $statement
	 * @return int
	 *
	 * @throws \PDOException
	 */
	public function exec(string $statement): int;

	/**
	 * Выполняет SQL-запрос и возвращает результирующий набор в виде объекта PDOStatement
	 */

	/**
	 * @param $statement
	 * @param $mode
	 * @param null $arg3
	 * @param array $ctorargs
	 * @return \PDOStatement
	 *
	 * @throws \PDOException
	 */
	public function query ($statement, $mode = \PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = array()) :PdoStatementInterface;

	/**
	 * Возвращает ID последней вставленной строки или значение последовательности
	 * @return int
	 */
	public function lastInsertId (): int;

	/**
	 * Заключает строку в кавычки для использования в запросе
	 */
	public function quote ($string, $parameter_type = \PDO::PARAM_STR) :string;
}