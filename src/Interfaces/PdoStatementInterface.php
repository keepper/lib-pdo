<?php
namespace Keepper\Lib\Pdo\Interfaces;

interface PdoStatementInterface {

	/**
	 * Запускает подготовленный запрос на выполнение
	 * @param array|null $inputParameters   Массив значений, содержащий столько элементов, сколько параметров заявлено в SQL-запросе.
	 * @return bool
	 */
	public function execute(array $inputParameters = null): bool;

	/**
	 * Извлечение следующей строки из результирующего набора
	 * @param null $fetch_style         Определяет, в каком виде следующая строка будет возвращена в вызывающий метод
	 * @param int $cursor_orientation   Для объектов PDOStatement представляющих прокручиваемый курсор, этот параметр определяет, какая строка будет возвращаться в вызывающий метод.
	 * @param int $cursor_offset        Для объектов PDOStatement, представляющих прокручиваемый курсор, параметр cursor_orientation которых принимает значение PDO::FETCH_ORI_ABS, эта величина означает абсолютный номер строки, которую необходимо извлечь из результирующего набора.
	 * @return mixed
	 */
	public function fetch($fetch_style = null, $cursor_orientation = \PDO::FETCH_ORI_NEXT, $cursor_offset = 0);

	/**
	 * Привязывает параметр запроса к переменной
	 * @param $parameter
	 * @param $variable
	 * @param int $data_type
	 * @param null $length
	 * @param null $driver_options
	 * @return bool
	 */
	public function bindParam($parameter, &$variable, $data_type = \PDO::PARAM_STR, $length = null, $driver_options = null): bool;

	/**
	 * Связывает столбец с переменной PHP
	 * @param $column
	 * @param $param
	 * @param null $type
	 * @param null $maxlen
	 * @param null $driverdata
	 * @return bool
	 */
	public function bindColumn($column, &$param, $type = null, $maxlen = null, $driverdata = null): bool;

	/**
	 * Связывает параметр с заданным значением
	 * @param $parameter
	 * @param $value
	 * @param int $data_type
	 * @return mixed
	 */
	public function bindValue($parameter, $value, $data_type = \PDO::PARAM_STR): bool;

	/**
	 * Возвращает количество строк, затронутых последним SQL-запросом
	 * @return int
	 */
	public function rowCount(): int;

	/**
	 * Возвращает данные одного столбца следующей строки результирующего набора
	 * @param int $column_number
	 * @return mixed
	 */
	public function fetchColumn($column_number = 0);

	/**
	 * Возвращает массив, содержащий все строки результирующего набора
	 * @param null $fetch_style
	 * @param null $fetch_argument
	 * @param array $ctor_args
	 * @return array
	 */
	public function fetchAll($fetch_style = null, $fetch_argument = null, array $ctor_args = array()): array;

	/**
	 * Извлекает следующую строку и возвращает ее в виде объекта
	 * @param string $class_name
	 * @param array $ctor_args
	 * @return mixed
	 */
	public function fetchObject($class_name = "stdClass", array $ctor_args = array());

	/**
	 * Устанавливает атрибут объекту PDOStatement
	 * @param $attribute
	 * @param $value
	 * @return bool
	 */
	public function setAttribute($attribute, $value): bool;

	/**
	 * Получение значения атрибута запроса PDOStatement
	 * @param $attribute
	 * @return mixed
	 */
	public function getAttribute($attribute);

	/**
	 * Возвращает количество столбцов в результирующем наборе
	 * @return int
	 */
	public function columnCount(): int;

	/**
	 * Возвращает метаданные столбца в результирующей таблице
	 * @param $column
	 * @return array
	 */
	public function getColumnMeta($column): array;

	/**
	 * Устанавливает режим выборки по умолчанию для объекта запроса
	 * @param $mode
	 * @param $classNameObject
	 * @param array $ctorarfg
	 * @return bool
	 */
	public function setFetchMode($mode, $classNameObject, array $ctorarfg): bool;

	/**
	 * Переход к следующему набору строк в результате запроса
	 * @return bool
	 */
	public function nextRowset(): bool;

	/**
	 * Закрывает курсор, переводя запрос в состояние готовности к повторному запуску
	 * @return mixed
	 */
	public function closeCursor(): bool;
}