<?php
namespace Keepper\Lib\Pdo;

use Keepper\Lib\Pdo\Exceptions\TransactionException;
use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Keepper\Lib\Pdo\Interfaces\PdoStatementInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class Pdo implements PdoInterface {

	use LoggerAwareTrait;

	/**
	 * @var \PDO
	 */
	private $pdo;

	private $isConnected = false;
	private $_transactionNestingLevel = 0;
	private $_isRollbackOnly = false;

	private $dsn;
	private $username;
	private $pwd;
	private $connOptions;

	public function __construct(string $dsn , string $username = null , string $passwd = null , array $options = [] ) {

		$this->connOptions = $opt = [
			\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
			//\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
			//\PDO::ATTR_EMULATE_PREPARES   => false,
		] + $options;
		$this->dsn = $dsn;
		$this->username = $username ?? 'root';
		$this->pwd = $passwd ?? '';
		$this->setLogger(new NullLogger());
	}

	public function setPdo(\PDO $pdo) {
		$this->isConnected = true;
		$this->pdo = $pdo;
		$this->_isRollbackOnly = false;
		$this->_transactionNestingLevel = (int) $pdo->inTransaction();
	}

	protected function connect() {
		if ( $this->isConnected ) {
			return;
		}

		$this->pdo = new \PDO($this->dsn, $this->username , $this->pwd, $this->connOptions);
		$this->isConnected = true;
	}

	/**
	 * @inheritdoc
	 */
	public function prepare($statement, array $driverOptions = array()): PdoStatementInterface {
		$this->connect();
		$result = call_user_func_array([$this->pdo, 'prepare'], func_get_args());
		return new PdoStatement($result);
	}

	/**
	 * @inheritdoc
	 */
	public function query($statement, $mode = \PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = array()): PdoStatementInterface {
		$this->connect();
		$result = call_user_func_array([$this->pdo, 'query'], func_get_args());
		return new PdoStatement($result);
	}

	/**
	 * @inheritdoc
	 */
	public function beginTransaction() {
		$this->connect();

		++$this->_transactionNestingLevel;

		if ($this->_transactionNestingLevel != 1) {
			return;
		}

		$this->logger->debug('"STARTING TRANSACTION"');
		$this->pdo->beginTransaction();
	}

	/**
	 * @inheritdoc
	 */
	public function commit() {
		if ($this->_transactionNestingLevel == 0) {
			throw new TransactionException('Нет активной транзакции для комита');
		}

		if ($this->_isRollbackOnly) {
			throw new TransactionException('Транзакция в режиме "только ролбэк" коммит не возможен');
		}

		$this->connect();

		if ($this->_transactionNestingLevel == 1) {
			$this->logger->debug('"COMMITING TRANSACTION"');
			$this->pdo->commit();
		}

		--$this->_transactionNestingLevel;
	}

	/**
	 * @inheritdoc
	 */
	public function rollBack() {
		if ($this->_transactionNestingLevel == 0) {
			throw new TransactionException('Нет активной транзакции для роллбэка');
		}

		$this->connect();

		if ($this->_transactionNestingLevel == 1) {
			$this->logger->debug('ROLLBACKING TRANSACTION');
			$this->_transactionNestingLevel = 0;
			$this->_isRollbackOnly = false;
			$this->pdo->rollBack();
		} else {
			$this->_isRollbackOnly = true;
			--$this->_transactionNestingLevel;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function inTransaction(): bool {
		$this->connect();
		return $this->pdo->inTransaction();
	}

	/**
	 * @inheritdoc
	 */
	public function setAttribute(int $attribute, $value): bool {
		$this->connect();
		return $this->pdo->setAttribute($attribute, $value);
	}

	/**
	 * @inheritdoc
	 */
	public function exec(string $statement): int {
		$this->connect();
		return $this->pdo->exec($statement);
	}

	/**
	 * @inheritdoc
	 */
	public function lastInsertId(): int {
		$this->connect();
		return (int) $this->pdo->lastInsertId();
	}

	/**
	 * @inheritdoc
	 */
	public function quote($string, $parameter_type = \PDO::PARAM_STR): string {
		$this->connect();
		return call_user_func_array([$this->pdo, 'quote'], func_get_args());
	}

}