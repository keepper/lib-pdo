<?php
namespace Keepper\Lib\Pdo;

use Keepper\Lib\Pdo\Interfaces\PdoStatementInterface;

class PdoStatement implements PdoStatementInterface {

	/**
	 * @var \PDOStatement
	 */
	private $statement;

	public function __construct(\PDOStatement $statement) {
		$this->statement = $statement;
	}

	/**
	 * @inheritdoc
	 */
	public function execute(array $inputParameters = null): bool {
		return call_user_func_array([$this->statement, 'execute'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function fetch($fetch_style = null, $cursor_orientation = \PDO::FETCH_ORI_NEXT, $cursor_offset = 0) {
		return call_user_func_array([$this->statement, 'fetch'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function bindParam($parameter, &$variable, $data_type = \PDO::PARAM_STR, $length = null, $driver_options = null): bool {
		return call_user_func_array([$this->statement, 'bindParam'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function bindColumn($column, &$param, $type = null, $maxlen = null, $driverdata = null): bool {
		return call_user_func_array([$this->statement, 'bindColumn'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function bindValue($parameter, $value, $data_type = \PDO::PARAM_STR): bool {
		return call_user_func_array([$this->statement, 'bindValue'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function rowCount(): int {
		return $this->statement->rowCount();
	}

	/**
	 * @inheritdoc
	 */
	public function fetchColumn($column_number = 0) {
		return $this->statement->fetchColumn($column_number);
	}

	/**
	 * @inheritdoc
	 */
	public function fetchAll($fetch_style = null, $fetch_argument = null, array $ctor_args = array()): array {
		return call_user_func_array([$this->statement, 'fetchAll'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function fetchObject($class_name = "stdClass", array $ctor_args = array()) {
		return call_user_func_array([$this->statement, 'fetchObject'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function setAttribute($attribute, $value): bool {
		return $this->statement->setAttribute($attribute, $value);
	}

	/**
	 * @inheritdoc
	 */
	public function getAttribute($attribute) {
		return $this->statement->getAttribute($attribute);
	}

	/**
	 * @inheritdoc
	 */
	public function columnCount(): int {
		return $this->statement->columnCount();
	}

	/**
	 * @inheritdoc
	 */
	public function getColumnMeta($column): array {
		return $this->statement->getColumnMeta($column);
	}

	/**
	 * @inheritdoc
	 */
	public function setFetchMode($mode, $classNameObject, array $ctorarfg): bool {
		return call_user_func_array([$this->statement, 'setFetchMode'], func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	public function nextRowset(): bool {
		return $this->statement->nextRowset();
	}

	/**
	 * @inheritdoc
	 */
	public function closeCursor(): bool {
		return $this->statement->closeCursor();
	}
}