<?php
namespace Keepper\Lib\Pdo\Tests;

use Keepper\Lib\Pdo\Exceptions\TransactionException;
use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Keepper\Lib\Pdo\Interfaces\PdoStatementInterface;
use Keepper\Lib\Pdo\Pdo;
use Monolog\Logger;

class PdoTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var \PDO|\PHPUnit_Framework_MockObject_MockObject
	 */
	private $realPdo;

	/**
	 * @var PdoInterface|Pdo
	 */
	private $pdo;

	public function setUp() {
		parent::setUp();
		$logger = new Logger('PDO');
		$this->realPdo = $this->getMockBuilder(\PDO::class)
			->disableOriginalConstructor()
			->setMethods([
				'prepare', 'setAttribute', 'exec', 'query', 'lastInsertId', 'quote',
				'beginTransaction', 'commit', 'rollBack', 'inTransaction'
			])->getMock();

		$this->pdo = new Pdo('');
		$this->pdo->setPdo($this->realPdo);
		$this->pdo->setLogger($logger);
	}

	/**
	 * @dataProvider dataProviderForPrepare
	 */
	public function testPrepare($query, $options) {
		$statement = $this->getMockBuilder(\PDOStatement::class)->disableOriginalConstructor()->getMock();

		$this->realPdo
			->expects($this->once())
			->method('prepare')
			->with($query, $options)
			->willReturn($statement);

		$result = $this->pdo->prepare($query, $options);
		$this->assertInstanceOf(PdoStatementInterface::class, $result);
	}

	public function dataProviderForPrepare() {
		return [
			['select * from users', []],
			['select * from customers', ['a' => 'b']]
		];
	}

	/**
	 * @dataProvider dataProviderForQuery
	 */
	public function testQuery($query, $mode, $arg3, $ctorargs) {
		$statement = $this->getMockBuilder(\PDOStatement::class)->disableOriginalConstructor()->getMock();

		$this->realPdo
			->expects($this->once())
			->method('query')
			->with($query, $mode, $arg3, $ctorargs)
			->willReturn($statement);

		$result = $this->pdo->query($query, $mode, $arg3, $ctorargs);
		$this->assertInstanceOf(PdoStatementInterface::class, $result);
	}

	public function dataProviderForQuery() {
		return [
			['select * from users', rand(0,10), null, []],
			['select * from customers', rand(0,10), 3, ['a'=>'b']],
		];
	}

	public function testStartAndRollbackTransaction() {
		$this->realPdo
			->expects($this->once())
			->method('beginTransaction');

		$this->realPdo
			->expects($this->once())
			->method('rollBack');

		$this->pdo->beginTransaction(); // First level
		$this->pdo->beginTransaction(); // Second level

		$this->pdo->rollBack();
		$this->pdo->rollBack();
	}

	/**
	 * @expectedException \Keepper\Lib\Pdo\Exceptions\TransactionException
	 * @dataProvider dataProviderForTransactionExceptionIfNotStarted
	 */
	public function testTransactionExceptionIfNotStarted($method) {
		$this->pdo->$method();
	}

	public function dataProviderForTransactionExceptionIfNotStarted() {
		return [
			['commit'],
			['rollBack'],
		];
	}

	/**
	 * @expectedException \Keepper\Lib\Pdo\Exceptions\TransactionException
	 */
	public function testTransactionExceptionIfCommitAfterRollback() {
		$this->pdo->beginTransaction(); // First level
		$this->pdo->beginTransaction(); // Second level

		$this->pdo->rollBack(); // Second level

		$this->pdo->commit(); // FirstLevel
	}

	public function testStartAndRCommitTransaction() {
		$this->realPdo
			->expects($this->once())
			->method('beginTransaction');

		$this->realPdo
			->expects($this->once())
			->method('commit');

		$this->pdo->beginTransaction(); // First level
		$this->pdo->beginTransaction(); // Second level

		$this->pdo->commit();
		$this->pdo->commit();
	}

	/**
	 * @dataProvider dataProviderForInTransaction
	 */
	public function testInTransaction($expected) {
		$this->realPdo
			->expects($this->once())
			->method('inTransaction')
			->willReturn($expected);

		$this->assertEquals($expected, $this->pdo->inTransaction());
	}

	public function dataProviderForInTransaction() {
		return [
			[true],
			[false]
		];
	}

	/**
	 * @dataProvider dataProviderForSetAttribute
	 */
	public function testSetAttribute($attr, $value, $expected) {
		$this->realPdo
			->expects($this->once())
			->method('setAttribute')
			->with($attr, $value)
			->willReturn($expected);

		$this->assertEquals($expected, $this->pdo->setAttribute($attr, $value));
	}

	public function dataProviderForSetAttribute() {
		return [
			[1, 'a', true],
			[2, 'b', false]
		];
	}

	/**
	 * @dataProvider dataProviderForExec
	 */
	public function testExec($attr, $expected) {
		$this->realPdo
			->expects($this->once())
			->method('exec')
			->with($attr)
			->willReturn($expected);

		$this->assertEquals($expected, $this->pdo->exec($attr));
	}

	public function dataProviderForExec() {
		return [
			['a', rand(10,100)],
			['b', rand(0,10)]
		];
	}

	/**
	 * @dataProvider dataProviderForLastInsertId
	 */
	public function testLastInsertId($expected) {
		$this->realPdo
			->expects($this->once())
			->method('lastInsertId')
			->with(null)
			->willReturn($expected);

		$this->assertEquals($expected, $this->pdo->lastInsertId());
	}

	public function dataProviderForLastInsertId() {
		return [
			[rand(10,100)],
			[rand(0,10)]
		];
	}

	/**
	 * @dataProvider dataProviderForQuote
	 */
	public function testQuote($attr, $value, $expected) {
		$this->realPdo
			->expects($this->once())
			->method('quote')
			->with($attr, $value)
			->willReturn($expected);

		$this->assertEquals($expected, $this->pdo->quote($attr, $value));
	}

	public function dataProviderForQuote() {
		return [
			['a', rand(10,100), 'aa'],
			['b', rand(0,10), 'bb']
		];
	}

	public function testRealConnect() {
		$dsn = getenv('test_dsn');
		$user = getenv('test_user');
		$passw = getenv('test_pwd');

		if ( is_null($dsn) || $dsn === false) {
			$this->markTestSkipped('Нет переменных окружения, с параметрами соединения с БД');
		}

		$pdo = new Pdo($dsn, $user, $passw);
		$result = $pdo->query('show tables');
		$rows = $result->fetchAll();
		$this->assertTrue(count($rows) > 0);
	}
}